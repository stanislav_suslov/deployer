const
    http = require('http'),
    axios = require('axios'),
    execa = require('execa'),
    fs = require('fs'),
    moment = require('moment');

const
    API = '548866417:AAEbBaOjnPly3BWuRlFU_kuRR_L32gUkEkk',
    path = '/var/www/moskva.dailycurrency.ru/public_html';

async function sendMessage(text) {
    return await axios.post(`https://api.telegram.org/bot${API}/sendMessage`, {
        chat_id: 67140340,
        parse_mode: 'HTML',
        text: text
    });
}

http.createServer((request, response) => {
    response.end();

    let requestArray = [];
    request.on('data', (chunk) => {
        requestArray.push(chunk);
    }).on('end', () => {
        fs.exists('./requests', (exists) => {
            if (!exists)
                fs.mkdirSync('./requests');
            fs.writeFileSync(`requests/${moment().format('YYYY-MM-DD-HH-MM-SS')}.json`, Buffer.concat(requestArray).toString());
        });
    });
    
    (async () => {
        const timeStart = new Date();
        await sendMessage('<b>MOSKVA</b> START');
        
        console.log(execa.shellSync('npm i', {cwd: path}).stdout);
        console.log(execa.shellSync('git pull', {cwd: path}).stdout);
        
        const timeEnd = new Date();
        const timeDiff = Math.round((timeEnd - timeStart)/1000);
        sendMessage(`<b>MOSKVA</b> END\nSpent ${timeDiff} seconds\nmoskva.dailycurrency.ru`);
    })();
}).listen(3030);
